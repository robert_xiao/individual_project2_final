FROM rust:latest as builder


WORKDIR /usr/src/sum_api


COPY . .


RUN cargo build --release


FROM debian:bookworm-slim



COPY --from=builder /usr/src/sum_api/target/release/sum_api .


RUN apt-get update \
    && apt-get install -y ca-certificates \
    && rm -rf /var/lib/apt/lists/*


EXPOSE 8080


CMD ["./sum_api"]

