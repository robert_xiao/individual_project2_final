use actix_web::{web, App, HttpResponse, HttpServer, Responder, HttpRequest};
use serde::Deserialize;

#[derive(Deserialize)]
struct SumParams {
    num1: i32,
    num2: i32,
}

async fn index() -> impl Responder {
    HttpResponse::Ok().content_type("text/html").body(
        r#"
            <html>
                <head><title>Sum Calculator</title></head>
                <body>
                    <form action="/calculate" method="post">
                        <input type="text" name="num1" placeholder="Enter a number"/>
                        <input type="text" name="num2" placeholder="Enter another number"/>
                        <button type="submit">Calculate Sum</button>
                    </form>
                </body>
            </html>
        "#,
    )
}

async fn calculate_sum(form: web::Form<SumParams>) -> impl Responder {
    let sum = form.num1 + form.num2;
    HttpResponse::Ok().body(format!("The sum of {} and {} is {}", form.num1, form.num2, sum))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(index)) 
            .route("/calculate", web::post().to(calculate_sum)) 
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
