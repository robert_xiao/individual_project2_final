# Individual_project2_final



## Overview：
To containerize a Rust web service, including containerizing a simple Rust Rest API, building Docker image, running container locally and make CI/CD Pipeline to Gitlab.<br>
## Requirements:
Simple REST API/web service in Rust<br>
Dockerfile to containerize service<br>
CI/CD pipeline files<br>
## Steps:
1.	Open cmd on Windows and use "cargo new sum_api" to create a new Rust project.<br>
2.	Go to /src/main.rs and create a function that enters two numbers input number boxes under the webpag, then click the Calculate Sum button, and then display the final result, if the input is not a number, it will display the input content is not digital. The code is shown below.<br>
![Alt text](image.png)

 
3.	In Cargo. toml, it should include the following.<br>
 ![Alt text](image-1.png)
4.	Create a corresponding Dockerfile like the following.<br>
 ![Alt text](image-2.png)

5.	Open the command prompt and navigate to the current directory. Then, run the command 'docker build -t sum_api .' <br>
Finally, execute the command 'docker run -d -p 8080:8080 sum_api'. Then, open Docker Desktop and navigate to the 'Containers' section. I can be able to run the container named 'wizardly_leakey'. <br>
 ![Alt text](image-3.png)
6.	I can see my web app under http://localhost:8080/ like the following.<br>
 ![Alt text](image-4.png)
 ![Alt text](image-5.png)
![Alt text](image-6.png)<br>
CI/CD Pipeline can be automated by uploading to Gitlab using the following files- .gitlab-ci.yml.<br>
 ![Alt text](image-7.png)<br>
CI/CD Pipeline screenshot:
![Alt text](image-8.png)
 
The Demo video is under the root directory. The video is called Demo_individual2.mp4<br>
